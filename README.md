# LKD70's Auto Join Ark
A simple Ark auto join simming tool for when lazyclicker is offline.

This tool isn't monetized, it doesn't require any sort of user account and I don't restrict who can use it.


## Getting Started

### Downloading
You can download the tool [Here](https://gitlab.com/LKD70/lkd-aja/raw/master/LKD-AJA.exe?inline=false).

### Install
The application is portable so doesn't require installing.

Once downloaded, put it in a folder named whatever you'd like and run it to start.


## FAQ
No one asks questions about a tool that isn't used, so I'll come up with some questions to fill this space.

### Q: How can I update the application?
#### A: Upon launch the application will check for updates and request you install if an update is available. To force an update, under the File menu, select "Check for Update".

### Q: My server isn't listed, what do??!?
#### A: Chances are, your server list is outdated. To update, in the File menu, select "Update server list".
Note: It's still possible that the external server list is also out of date. I'm lazy.

### Q: How can I change which version of Ark the application launches?
#### A: File menu -> Change Ark exe

## Contact
If for some reason you feel the need to contact me, here's a few methods to do so:
* [Telegram](https://t.me/LKD70)
* Discord: LKD70 (Luke)#7070