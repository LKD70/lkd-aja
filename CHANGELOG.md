# Changes

## Version 6
* Removed unneeded click.

## Version 5
* Bug fix.

## Version 4
* Added command line paramater options.

## Version 3
* Fixed a typo resulting in the game not launching

## Version 2
* Fixed auto updater
* Added menu
* Added server list update option to menu
* Added manual update option to menu
* Added method to prevent multiple sims being launched (hopefully)
